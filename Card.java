public class Card {
	private String suit;
	private int value;
	
	public Card (String suit, int value) {
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit() {
		return this.suit;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public String toString() {
		String printValue = "";
		if (value == 13) {
			printValue = "King";
		} else if (value == 12) {
			printValue = "Queen";
		} else if (value == 11) {
			printValue = "Jack";
		} else if (value == 1) {
			printValue = "Jack";
		} else {
			printValue = Integer.toString(value);
		}
		return printValue + " of " + suit;
	}
	
	public double calculateScore() {
		double decimalValue = this.value + 0.0;
		double suitValue;
		if (this.suit.equals("Hearts")) {
			suitValue = 0.4;
		} else if (this.suit.equals("Spades")) {
			suitValue = 0.3;
		} else if (this.suit.equals("Diamonds")) {
			suitValue = 0.2;
		} else {
			suitValue = 0.1;
		}
		double score = decimalValue + suitValue;
		return score;
	}
}