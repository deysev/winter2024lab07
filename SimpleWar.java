public class SimpleWar {
	public static void main(String[] args) {
		Deck stack = new Deck();
		stack.shuffle();	
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		int round = 1;
		while (stack.length() > 0) {
			Card cardOne = stack.drawTopCard();
			Card cardTwo = stack.drawTopCard();
			System.out.println(cardOne);
			System.out.println(cardTwo);
		
			double playerOneScore = cardOne.calculateScore();
			double playerTwoScore = cardTwo.calculateScore();
			System.out.println("Player 1 Score: " + playerOneScore);
			System.out.println("Player 2 Score: " + playerTwoScore);
			
			if (playerOneScore < playerTwoScore) {
				System.out.println("Player 2 wins round " + round + " !");
				playerTwoPoints++;
			} else {
				System.out.println("Player 1 wins round " + round + " !");
				playerOnePoints++;
			}
			round++;
			System.out.println("*******************************************************");
		}
		
		//Final Winner
		if (playerOnePoints < playerTwoPoints) {
			System.out.println("Player 2 WINS!!! CONGRATS!");
		} else {
			System.out.println("Player 1 WINS!!! CONGRATS!");
		}
	}
}